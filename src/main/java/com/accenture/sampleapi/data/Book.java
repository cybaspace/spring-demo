package com.accenture.sampleapi.data;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "books")
public class Book {
    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long id;
    private @Column(unique = true) String isbn;
    private String title;
    private String author;


}
